from datetime import datetime

class Geocache:
    """Class with information about a Geocache and it log status."""

    def __init__(self, gc_code, link, found_status, date, time, title, note):
        self.gcCode = gc_code
        self.link = link
        self.foundStatus = found_status
        self.date = date
        self.time = time
        self.title = title
        self.note = note

    def get_csv_date_string(self):
        mix = self.date + ' ' + self.time
        d = datetime.strptime(mix, '%d %b %y %H:%M')
        return d.strftime('%Y-%m-%dT%H:%MZ')

    def get_csv_string(self):
        return self.gcCode + ',' + self.get_csv_date_string() + ',' + self.foundStatus[:-1] + ',\"' + self.note + '\"'
