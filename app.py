import re
from bs4 import BeautifulSoup
from models import *

with open("/home/enno/Downloads/Drafts.html", "r") as fp:
    document = fp.read()

soup = BeautifulSoup(document, 'html.parser')

geocaches = []

for item in soup.body.find_all('li', attrs={'class': 'draft-item'}):
    itemparser = BeautifulSoup(str(item), 'html.parser')
    gc_code = re.search('GC[A-Z0-9]{2,5}', str(itemparser.div.a.get('href'))).group()
    geocaches.append(geocache.Geocache(
        gc_code,
        itemparser.div.a.get('href'),
        itemparser.div.a.dt.getText(),
        itemparser.div.a.dd.span.getText(),
        itemparser.div.a.dd.span.next_sibling.next_sibling.getText(),
        itemparser.div.a.h2.getText(),
        itemparser.div.a.p.getText())
    )

for cache in geocaches:
    print(cache.get_csv_string())
